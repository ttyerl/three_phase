defmodule CohortTest do
  use ExUnit.Case
  require Logger
  
  doctest Cohort

  # seed: 0  # to remove randomize test cases
  
  setup do
    {:ok, pid} = Cohort.start_link()
    {:ok, [simple_cohort: pid]}
  end
  
  test "Updating a value", context do
    pid = context[:simple_cohort]

    Cohort.request(pid, "hello")
    Logger.info(:io_lib.format('1: ~p', [:sys.get_state(pid)]))
    assert_received {:"$gen_cast", {:agree, pid}}

    Cohort.prepare(pid)
    Logger.info(:io_lib.format('2: ~p', [:sys.get_state(pid)]))
    assert_received {:"$gen_cast", {:ack, pid}}

    Cohort.commit(pid)
    Logger.info(:io_lib.format('3: ~p', [:sys.get_state(pid)]))
    
    data = Cohort.get_data(pid)
    assert data == <<"hello">>
  end

  test "Aborting after commit request", context do
    pid = context[:simple_cohort]

    {current_state, process_state} = :sys.get_state(pid)
    :sys.replace_state(pid, fn _ -> {current_state, %{process_state | data: <<"fake">>}} end)
    Logger.info(:io_lib.format('4: ~p', [:sys.get_state(pid)]))
    
    data = Cohort.get_data(pid)
    assert data == <<"fake">>

    Cohort.request(pid, "world")
    Logger.info(:io_lib.format('5: ~p', [:sys.get_state(pid)]))
    assert_received {:"$gen_cast", {:agree, pid}}

    Cohort.abort(pid)
    Logger.info(:io_lib.format('6: ~p', [:sys.get_state(pid)]))

    data0 = Cohort.get_data(pid)
    assert data0 == <<"fake">>
  end

  test "Aborting after prepared request", context do
    pid = context[:simple_cohort]

    {current_state, process_state} = :sys.get_state(pid)
    :sys.replace_state(pid, fn _ -> {current_state, %{process_state | data: <<"fake">>}} end)
    Logger.info(:io_lib.format('7: ~p', [:sys.get_state(pid)]))
    
    data = Cohort.get_data(pid)
    assert data == <<"fake">>

    Cohort.request(pid, "world")
    Logger.info(:io_lib.format('8: ~p', [:sys.get_state(pid)]))
    assert_received {:"$gen_cast", {:agree, pid}}

    Cohort.prepare(pid)
    Logger.info(:io_lib.format('9: ~p', [:sys.get_state(pid)]))
    assert_received {:"$gen_cast", {:ack, pid}}

    Cohort.abort(pid)
    Logger.info(:io_lib.format('10: ~p', [:sys.get_state(pid)]))

    data0 = Cohort.get_data(pid)
    assert data0 == <<"fake">>
  end
end
