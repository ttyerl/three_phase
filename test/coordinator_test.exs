defmodule CoordinatorTest do
  use ExUnit.Case
  require Logger

  doctest Coordinator

  setup do
    {:ok, cohort1} = Cohort.start_link()
    {:ok, cohort2} = Cohort.start_link()
    {:ok, pid} = Coordinator.start_link()
    
    {:ok, [cohort1: cohort1, cohort2: cohort2, coordinator: pid]}
  end

  test "Adding cohorts", context do
    pid = context[:coordinator]
    cohort1 = context[:cohort1]
    cohort2 = context[:cohort2]
    Coordinator.add_cohort(pid, cohort1)
    Coordinator.add_cohort(pid, cohort2)

    {:ready, %Coordinator{
        agreed: [],
        cohorts: cohorts,
          timeout: 3000,
          timer: nil
        }} =  :sys.get_state(pid)
    assert true == Enum.member?(cohorts, cohort1)
    assert true == Enum.member?(cohorts, cohort2)
  end

  test "Commiting data to cohorts", context do
    pid = context[:coordinator]
    cohort1 = context[:cohort1]
    cohort2 = context[:cohort2]
    Coordinator.add_cohort(pid, cohort1)
    Coordinator.add_cohort(pid, cohort2)

    Coordinator.commit_data(pid, "hello world")
    :timer.sleep(5)
    assert {:ready, %Cohort{data: "hello world", temp_data: nil}} == :sys.get_state(cohort1)
    assert {:ready, %Cohort{data: "hello world", temp_data: nil}} == :sys.get_state(cohort2)
    
  end
end
