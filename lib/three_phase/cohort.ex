require Logger

defmodule Cohort do
  use GenStateMachine, callback_mode: :state_functions

  defstruct data: nil, temp_data: nil

  def start_link do
    GenStateMachine.start_link(Cohort, [])
  end

  def request(to, data) do
    GenStateMachine.cast(to, {:commit_request, self(), data})
  end
  
  def prepare(to) do
    GenStateMachine.cast(to, {:prepare, self()})
  end

  def commit(to) do
    GenStateMachine.cast(to, :commit)
  end

  def abort(to) do
    GenStateMachine.cast(to, :abort)
  end

  def get_data(from) do
    GenStateMachine.call(from, :get_data)
  end
  
  def init(_) do
    {:ok, :ready, %Cohort{}}
  end

  def ready(:cast, {:commit_request, from, data}, state) do
    Coordinator.send_agreement(from, self())
    {:next_state, :commit_request_req, %{state | temp_data: data}}
  end
  def ready(:cast, {:commit_request_reject, from, _data}, state) do
    Coordinator.send_disagreement(from, self())
    {:next_state, :ready, state}
  end
  def ready(event, message, state) do
    handle_event(event, message, :ready, state)
  end

  def commit_request_req(:cast, :abort, state) do
    {:next_state, :ready, %{state | temp_data: nil}}
  end
  def commit_request_req(:cast, {:prepare, from}, state) do
    Coordinator.send_ack(from, self())
    {:next_state, :prepare_req, state}
  end
  def commit_request_req(event, message, state) do
    handle_event(event, message, :commit_request_req, state)
  end

  def prepare_req(:cast, :commit, state = %Cohort{temp_data: tdata}) do
    {:next_state, :ready, %{state | data: tdata, temp_data: nil}}
  end
  def prepare_req(:cast, :abort, state) do
    {:next_state, :ready, %{state | temp_data: nil}}
  end
  def prepare_req(event, message, state) do
    handle_event(event, message, :prepare_req, state)
  end

  def handle_event(:cast, message, statem, state) do
    Logger.info("Ignoring: received cast message #{message} in state #{state}")
    {:next_state, statem, state}
  end
  def handle_event({:call, from}, :get_data, statem, state = %Cohort{data: d}) do
    {:next_state, statem, state, [{:reply, from, d}]}
  end
end
