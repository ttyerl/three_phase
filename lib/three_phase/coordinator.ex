require Logger

defmodule Coordinator do
  use GenStateMachine, callback_mode: :state_functions

  defstruct cohorts: [], timeout: 3000, agreed: [], timer: nil 

  def start_link() do
    GenStateMachine.start_link(Coordinator, [])
  end
  
  def add_cohort(coordinator, cohort) do
    GenStateMachine.call(coordinator, {:add_cohort, cohort})
  end

  def commit_data(coordinator, data) do
    GenStateMachine.cast(coordinator, {:commit_data, data})
  end

  def send_agreement(pid, cohort) do
    GenStateMachine.cast(pid, {:agree, cohort})
  end

  def send_disagreement(pid, cohort) do
    GenStateMachine.cast(pid, {:disagree, cohort})
  end

  def send_ack(pid, cohort) do
    GenStateMachine.cast(pid, {:ack, cohort})
  end
  
  def init(_) do
    Logger.info("Init Coordinator")
    {:ok, :ready, %Coordinator{}}
  end

  def ready({:call, from}, {:add_cohort, cohort}, state = %Coordinator{cohorts: cohorts}) do
    new_state = %{state | cohorts: [cohort | cohorts]}
    {:keep_state, new_state, {:reply, from, new_state}}
  end
  def ready(:cast, {:commit_data, data}, state = %Coordinator{timeout: timeout, cohorts: cohorts}) do
    Logger.info("sending commit_data")
    # need to set timer in case cohorts take too long
    timerRef = Process.send_after(self(), :timeout, timeout)
    for cohort <- cohorts, do: Cohort.request(cohort, data)
    {:next_state, :commit_request_sent, %{state | timer: timerRef}}
  end

  def commit_request_sent({:call, from}, {:add_cohort, _}, state) do
    {:next_state, :commit_request_sent, state, {:reply, from, {:not_available, "Perform add_cohort request later."}}}
  end
  def commit_request_sent(:cast, {:agree, cohort}, state = %Coordinator{agreed: agreed, cohorts: cohorts,
									timer: oldTimerRef, timeout: timeout}) do
    if Enum.member?(cohorts, cohort) and not(Enum.member?(agreed, cohort)) do
      Logger.info("adding cohort into agreed list")
      if length(cohorts) == (1 + length(agreed)) do
	# received agreements from all cohort

	perform_prepare_request(cohorts, oldTimerRef)
	
	# setup another timeout for acks
	timerRef = Process.send_after(self(), :timeout, timeout)
	{:next_state, :wait_for_ack, %{state | agreed: [], timer: timerRef}}
      else
	{:next_state, :commit_request_sent, %{state | agreed: [cohort | agreed]}}
      end
    else
      {:next_state, :commit_request_sent, state}
    end
  end
  def commit_request_sent(:cast, {:disagree, _cohort}, state = %Coordinator{cohorts: cohorts,
									    timer: oldTimerRef}) do
    perform_abort_request(cohorts, oldTimerRef)
    {:next_state, :ready, %{state | timer: nil, agreed: []}}
  end
  def commit_request_sent(:info, :timeout, state = %Coordinator{cohorts: cohorts,
								timer: oldTimerRef}) do
    # send abort msg to all cohorts
    Logger.info("timeout occured")
    perform_abort_request(cohorts, oldTimerRef)
    {:next_state, :ready, %{state | agreed: [], timer: nil}}
  end

  def wait_for_ack({:call, from}, {:add_cohort, _}, state) do
    {:next_state, :wait_for_ack, state, {:reply, from, {:not_available, "Perform add_cohort request later."}}}
  end
  def wait_for_ack(:cast, {:ack, cohort}, state = %Coordinator{agreed: agreed, cohorts: cohorts,
									timer: oldTimerRef}) do
    if Enum.member?(cohorts, cohort) and not(Enum.member?(agreed, cohort)) do
      Logger.info("adding cohort into agreed list for wait_for_ack")
      if length(cohorts) == (1 + length(agreed)) do
	# received ack from all cohort
	perform_commit_request(cohorts, oldTimerRef)
	{:next_state, :ready, %{state | agreed: [], timer: nil}}
      else
	{:next_state, :wait_for_ack, %{state | agreed: [cohort | agreed]}}
      end
    else
      {:next_state, :wait_for_ack, state}
    end
  end
  def wait_for_ack(:info, :timeout, state = %Coordinator{cohorts: cohorts, timer: oldTimerRef}) do
    # timeout waiting for ack, send abort to all cohorts
    perform_abort_request(cohorts, oldTimerRef)
    new_state = %{state | agreed: [], timer: nil}
    {:next_state, :ready, new_state}
  end

  defp perform_prepare_request(cohorts, timer) do
    # cancel old timer
    Process.cancel_timer(timer)
    # send commit msg to all cohorts
    for cohort <- cohorts, do: Cohort.prepare(cohort)
  end
  
  defp perform_commit_request(cohorts, timer) do
    # cancel old timer
    Process.cancel_timer(timer)
    # send commit msg to all cohorts
    for cohort <- cohorts, do: Cohort.commit(cohort)
  end

  defp perform_abort_request(cohorts, timer) do
    # cancel old timer
    Process.cancel_timer(timer)
    # send abort msg to all cohorts
    for cohort <- cohorts, do: Cohort.abort(cohort)
  end
  
end
