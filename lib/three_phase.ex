defmodule ThreePhase do
  @moduledoc """
  Documentation for ThreePhase.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ThreePhase.hello()
      :world

  """
  def hello do
    :world
  end
end
