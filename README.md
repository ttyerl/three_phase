# ThreePhase

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `three_phase` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:three_phase, "~> 0.1.0"}
  ]
end
```

```elixir
{ok, pid} = GenStateMachine.start_link(Coordinator, [])
GenStateMachine.call(pid, {:add_cohort, self()})
GenStateMachine.cast(pid, {:commit_data, "hello wrold"})
```

The Cohort unit test rely on test order

```
mix test --seed 0
```

to run tests in sequence.

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/three_phase](https://hexdocs.pm/three_phase).

